const express = require("express");
const router = express.Router(); 
const bodyparse = require("body-parser");

let datos = [{
    Matricula: "2020030663",
    Nombre: "Marvin Alejandro Lopez Diaz",
    Sexo: "M",
    Materias: ["Ingles","Base de datos","Tecnologias 1"]
},{
    Matricula: "2020030650",
    Nombre: "Alfonso Belazcar Cortes de las torres",
    Sexo: "M",
    Materias: ["Ingles","Base de datos","Tecnologias 1"]

},{
    Matricula: "2020030650",
    Nombre: "Felicia Martinoles Consepcion Diaz",
    Sexo: "F",
    Materias: ["Ingles","Base de datos","Tecnologias 1"]
}];

router.get("/",(req,res)=>{
    res.render("index.html",{titulo: "Mi primer pajina en Ejs", nombre: "Marvin Alejnadro Lopez Diaz", grupo: "8-3", Listado: datos});
});

router.get('/tabla',(req,res)=>{
    const params = {
        numero: req.query.numero
    }
    res.render('tabla.html',params);
})

router.post('/tabla',(req,res)=>{
    const params = {
        numero:req.body.numero
    }
    res.render('tabla.html',params);
})

router.get('/cotizacion',(req,res)=>{
    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazos: req.query.plazos
    }
    res.render('cotizacion.html',params);
})

router.post('/cotizacion',(req,res)=>{
    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazos: req.body.plazos
    }
    res.render('cotizacion.html',params);
})

module.exports = router;